﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Model;
using IO.Swagger.Api;
using IO.Swagger.Client;
using DemoConnector.Pruebas;
using DemoConnector.Properties;


namespace DemoConnector.Resumenbajatesst
{
  public  class Caso1ComBajasTestCase
    {
        public static ResumenBaja getDataTestCase() 
        {

            /*************emisor**********************/
            Emisor emisor = new Emisor(
            NombreComercial: "Asociacion Circulo Militar del Peru",
            NombreLegal: "ASOCIACION CIRCULO MILITAR DEL PERU",
                TipoDocumentoIdentidad: Emisor.TipoDocumentoIdentidadEnum.Ruc,
                NumeroDocumentoIdentidad: "10461633876",


                Correo: "sergio.conez@close2u.pe",
                DomicilioFiscal: new DomicilioFiscal(

                Direccion: "AV. SALAVERRY NRO. 1650 - Lima Lima  Jesús María",
                Urbanizacion: "",
                Provincia: "Lima",
                Distrito: "Jesus Maria",
                Departamento: "Lima",
                Pais: DomicilioFiscal.PaisEnum.Peru
                )

            );
            /****************Receptor********************/
            Receptor receptor = new Receptor(
                  NombreComercial: "luis Menage S.A.C",
                  NombreLegal: "luis Menage S.A.C",

                  TipoDocumentoIdentidad: Receptor.TipoDocumentoIdentidadEnum.Ruc,
                  NumeroDocumentoIdentidad: "10461633876",
                  Correo: "sergio.conez@close2u.pe",
                  DomicilioFiscal: new DomicilioFiscal(

                      Direccion: "CALLE CAHUIDE Nº900  BLOCK 19 DPTO ",
                      Urbanizacion: "URB. SAN FELIPE",
                      Provincia: "Lima",
                      Distrito: "JESUS MARIA",
                      Departamento: "Lima",
                      Pais: DomicilioFiscal.PaisEnum.Peru

                      )
                  );
            InformacionAdicional informacion = new InformacionAdicional();
            informacion.TipoOperacion = InformacionAdicional.TipoOperacionEnum.VentaInterna;

            ResumenBaja resumenBaja = new ResumenBaja(Close2u: new Integracion("C2U-2",
                                            Integracion.TipoIntegracionEnum.Erp, null,
                                            Integracion.TipoRegistroEnum.SinIgv

                    ),
                DatosDocumento: new Principal(FechaEmision: "2016-11-07",
                                                Serie: "FF13", Numero: 5,
                                                Moneda: Principal.MonedaEnum.Pen
                                                ),
                 Emisor: emisor,
                 Receptor: receptor,
                DetalleDocumento: new List<DocumentoItem>(),
                InformacionAdicional: informacion);


            DocumentoReferencia documentoreferencia = new DocumentoReferencia(Serie: "FF13", Numero: "5", TipoDocumento: DocumentoReferencia.TipoDocumentoEnum.Boleta);
           
          
          
         

            /***************Inicio detalle  del comunicado de bajas************************************/
            ResumenBajaItem resumenBajaItem1 = new ResumenBajaItem();
            resumenBajaItem1.TipoComprobante = "01";
      
            resumenBajaItem1.Numero = 11;
            resumenBajaItem1.Motivo = ("Error  en la factura  emitida");
            resumenBajaItem1.Serie = "FF13";
        resumenBajaItem1.Numero = 22;
            resumenBaja.ResumenBajaItemList = new List<ResumenBajaItem>();

            resumenBaja.ResumenBajaItemList.Add(resumenBajaItem1);

            return resumenBaja;
        }
}
}