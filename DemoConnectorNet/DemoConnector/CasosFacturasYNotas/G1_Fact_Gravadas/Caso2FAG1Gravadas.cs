﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Model;
using IO.Swagger.Api;
using IO.Swagger.Client;
using DemoConnector.Pruebas;
using DemoConnector.Properties;
namespace DemoConnector.G1_Grav_FF11
{
   public class Caso2FAG1Gravadas
    {
        public static Factura getCase2FAG1GRAV()
        {
            /******************Emisor*********************************/
            Emisor emisor = new Emisor(
               
        NombreComercial: "Asociacion Circulo Militar del Peru",
                        NombreLegal: "ASOCIACION CIRCULO MILITAR DEL PERU",

                            TipoDocumentoIdentidad: Emisor.TipoDocumentoIdentidadEnum.Ruc,
                            NumeroDocumentoIdentidad: "10461633876",
                            Correo: "sergio.conez@close2u.pe",
                            DomicilioFiscal: new DomicilioFiscal(
                            Direccion: "AV. SALAVERRY NRO. 1650 - Lima Lima  Jesús María",
                            Urbanizacion: "",
                            Provincia: "Lima",
                            Distrito: "Jesus Maria",
                            Departamento: "Lima",
                            Pais: DomicilioFiscal.PaisEnum.Peru
                            )
                        );


          


            InformacionAdicional informacion = new InformacionAdicional();
            informacion.TipoOperacion = InformacionAdicional.TipoOperacionEnum.VentaInterna;
            /****************Receptor********************/
            Receptor receptor = new Receptor(
                  NombreComercial: "luis Menage S.A.C",
                  NombreLegal: "luis Menage S.A.C",

                  TipoDocumentoIdentidad: Receptor.TipoDocumentoIdentidadEnum.Ruc,
                  NumeroDocumentoIdentidad: "10461633876",
                  Correo: "sergio.conez@close2u.pe",
                  DomicilioFiscal: new DomicilioFiscal(

                      Direccion: "CALLE CAHUIDE Nº900  BLOCK 19 DPTO ",
                      Urbanizacion: "URB. SAN FELIPE",
                      Provincia: "Lima",
                      Distrito: "JESUS MARIA",
                      Departamento: "Lima",
                      Pais: DomicilioFiscal.PaisEnum.Peru

                      )
                  );



            /******************PRINCIPAL***************************************/

            Factura factura = new Factura(Close2u: new Integracion("C2U-2",
                                            Integracion.TipoIntegracionEnum.Erp,null,
                                            //igv
                                            Integracion.TipoRegistroEnum.SinIgv),
                DatosDocumento: new Principal(FechaEmision: "2016-11-07",
                                                Serie: "FF03", Numero: 5,
                                                Moneda: Principal.MonedaEnum.Pen)
           ,
                Emisor: emisor,
               Receptor:receptor,
                DetalleDocumento: new List<DocumentoItem>(),
                InformacionAdicional: informacion);
            /*****************Totales********************/
            //descuentoGlobal
            factura.DescuentoGlobal = 0.0;
            factura.DetalleDocumento = new List<DocumentoItem>();
            /********************Inicio item1***************************/
            DocumentoItem facItem1 = new DocumentoItem(
                UnidadMedida: DocumentoItem.UnidadMedidaEnum.Caja,
                NumeroOrden: factura.DetalleDocumento.Count + 1,
                Cantidad: 3.0,
                Descripcion: "gaseosa coca",
                ValorVentaUnitarioItem: 27.60,
                CodigoProducto: "PROD1",
                
                TipoAfectacion: DocumentoItem.TipoAfectacionEnum.GravadoOperacionOnerosa
               

                );
         

            factura.DetalleDocumento.Add(facItem1);
            return factura;
        }
    }
}

