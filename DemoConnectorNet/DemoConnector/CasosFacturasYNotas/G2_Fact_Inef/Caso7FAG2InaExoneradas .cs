﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Model;
using IO.Swagger.Api;
using IO.Swagger.Client;
using DemoConnector.Pruebas;
using DemoConnector.Properties;
namespace DemoConnector.CasosFacturasYNotas.G2_Ina_FF12
{
 public   class Caso7FAG2InaExoneradas
    {
        public static NotaCredito getCasePrueba7FAInaExo()
        {

            /*********************Emisor**************************************/
            Emisor emisor = new Emisor(
                        NombreComercial: "Asociacion Circulo Militar del Peru",
                        NombreLegal: "ASOCIACION CIRCULO MILITAR DEL PERU",

                            TipoDocumentoIdentidad: Emisor.TipoDocumentoIdentidadEnum.Ruc,
                            NumeroDocumentoIdentidad: "10461633876",
                            Correo: "sergio.conez@close2u.pe",
                            DomicilioFiscal: new DomicilioFiscal(

                            Direccion: "AV. SALAVERRY NRO. 1650 - Lima Lima  Jesús María",
                            Urbanizacion: "",
                            Provincia: "Lima",
                            Distrito: "Jesus Maria",
                            Departamento: "Lima",
                            Pais: DomicilioFiscal.PaisEnum.Peru
                            )
                        );


            /****************Receptor********************/
            Receptor receptor = new Receptor(
                  NombreComercial: "luis Menage S.A.C",
                  NombreLegal: "luis Menage S.A.C",

                  TipoDocumentoIdentidad: Receptor.TipoDocumentoIdentidadEnum.Ruc,
                  NumeroDocumentoIdentidad: "10461633876",
                  Correo: "sergio.conez@close2u.pe",
                  DomicilioFiscal: new DomicilioFiscal(

                      Direccion: "CALLE CAHUIDE Nº900  BLOCK 19 DPTO ",
                      Urbanizacion: "URB. SAN FELIPE",
                      Provincia: "Lima",
                      Distrito: "JESUS MARIA",
                      Departamento: "Lima",
                      Pais: DomicilioFiscal.PaisEnum.Peru

                      )
                  );


            InformacionAdicional informacion = new InformacionAdicional();
            informacion.TipoOperacion = InformacionAdicional.TipoOperacionEnum.VentaInterna;

            NotaCredito notacredito = new NotaCredito(Close2u: new Integracion("C2U-2",
                                            Integracion.TipoIntegracionEnum.Erp, null,
                                            Integracion.TipoRegistroEnum.SinIgv
                                            ),
                DatosDocumento: new Principal(FechaEmision: "2016-11-07",
                                                Serie: "FF13", Numero: 5,
                                                Moneda: Principal.MonedaEnum.Pen,
                                                //motivos de la emision de nota de credito
                                                Glosa: "Motivos de emision de nota de credito")
           ,
                Emisor: emisor,
                Receptor:receptor,
                DetalleDocumento: new List<DocumentoItem>(),
                InformacionAdicional: informacion);
            /*****************Totales********************/
            DocumentoReferencia documentorefe = new DocumentoReferencia(Serie: "FF13", Numero: "5", TipoDocumento: DocumentoReferencia.TipoDocumentoEnum.Factura);
            //descuentoGlobal
            notacredito.ComprobanteAjustado = documentorefe;
            notacredito.Motivo = (NotaCredito.MotivoEnum.DescuentoPorItem);
            notacredito.DescuentoGlobal = 0.0;
            notacredito.DetalleDocumento = new List<DocumentoItem>();
            /********************Inicio item1***************************/
            DocumentoItem facItem1 = new DocumentoItem(
                UnidadMedida: DocumentoItem.UnidadMedidaEnum.Caja,
                NumeroOrden: notacredito.DetalleDocumento.Count + 1,
                Cantidad: 10.0,
                Descripcion: "Gloria UMT",
                ValorVentaUnitarioItem: 38.10,
                CodigoProducto: "PROD1",
                //factura con tipo de afectacion inafecto
                TipoAfectacion: DocumentoItem.TipoAfectacionEnum.InafectoOperacionOnerosa



                );

            notacredito.DetalleDocumento.Add(facItem1);
            return notacredito;
        }
    }
}
