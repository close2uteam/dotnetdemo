﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Model;
using IO.Swagger.Api;
using IO.Swagger.Client;
using DemoConnector.Pruebas;
using DemoConnector.Properties;
namespace DemoConnector.CasosFacturasYNotas.G3_Grat_FF13
{
  public  class Caso6FAGratuitas
    {
        public static Factura getCasePrueba6FAGratui()
        {

            /********************Emisor**************************************/

            Emisor emisor = new Emisor(
                        NombreComercial: "Asociacion Circulo Militar del Peru",
                        NombreLegal: "ASOCIACION CIRCULO MILITAR DEL PERU",

                            TipoDocumentoIdentidad: Emisor.TipoDocumentoIdentidadEnum.Ruc,
                            NumeroDocumentoIdentidad: "10461633876",
                            Correo: "sergio.conez@close2u.pe",
                            DomicilioFiscal: new DomicilioFiscal(

                            Direccion: "AV. SALAVERRY NRO. 1650 - Lima Lima  Jesús María",
                            Urbanizacion: "",
                            Provincia: "Lima",
                            Distrito: "Jesus Maria",
                            Departamento: "Lima",
                            Pais: DomicilioFiscal.PaisEnum.Peru
                            )
                        );


            /*****************Receptor********************/
            Receptor receptor = new Receptor(
                      NombreComercial: "Negociaciones Harol S.E.I.R.L",
                      NombreLegal: "Negociaciones Harol",

                      TipoDocumentoIdentidad: Receptor.TipoDocumentoIdentidadEnum.Ruc,
                      NumeroDocumentoIdentidad: "10461633876",
                      Correo: "sergio.conez@close2u.pe",
                      DomicilioFiscal: new DomicilioFiscal(

                          Direccion: "CALLE CAHUIDE Nº900  BLOCK 19 DPTO ",
                          Urbanizacion: "URB. SAN FELIPE",
                          Provincia: "Lima",
                          Distrito: "JESUS MARIA",
                          Departamento: "Lima",
                          Pais: DomicilioFiscal.PaisEnum.Peru

                          )
                      );


            /*******************Principal*****************************************/

            InformacionAdicional informacion = new InformacionAdicional();
            informacion.TipoOperacion = InformacionAdicional.TipoOperacionEnum.VentaInterna;

            Factura factura = new Factura(Close2u: new Integracion("C2U-2",
                                            Integracion.TipoIntegracionEnum.Erp, null,
                                            Integracion.TipoRegistroEnum.SinIgv
                                            ),
                DatosDocumento: new Principal(FechaEmision: "2016-11-07",
                                                Serie: "FF13", Numero: 0204,
                                                Moneda: Principal.MonedaEnum.Pen,
                                                //motivos de la emision de nota de credito
                                                Glosa: "Motivos de emision de nota de credito")
           ,
                Emisor: emisor,
                Receptor: receptor,
                DetalleDocumento: new List<DocumentoItem>(),
                InformacionAdicional: informacion);
            /*****************Totales********************/
            //descuentoGlobal
            factura.DescuentoGlobal = 0.0;
            factura.DetalleDocumento = new List<DocumentoItem>();
            /********************Inicio item1***************************/
            DocumentoItem facItem1 = new DocumentoItem(
                UnidadMedida: DocumentoItem.UnidadMedidaEnum.Caja,
                NumeroOrden: factura.DetalleDocumento.Count + 1,
                Cantidad: 1.0,
                Descripcion: "COMISION  POR VIA CONCES BANCA JULIO 2016",
                ValorVentaUnitarioItem: 522.36,
                CodigoProducto: "PROD1",
                //factura con tipo de afectacion Gratuita
                TipoAfectacion: DocumentoItem.TipoAfectacionEnum.ExoneradoTransferenciaGratuita
                
                );

            factura.DetalleDocumento.Add(facItem1);
            return factura;
        }
    }
}
