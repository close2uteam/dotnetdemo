﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Model;
using IO.Swagger.Api;
using IO.Swagger.Client;
using DemoConnector.Pruebas;
using DemoConnector.Properties;
namespace DemoConnector.CasosFacturasYNotas.G9_FacExport_FF70
{
 public   class Caso36_FAExpFF70
    {
        public static Factura getCasePrueba36_FAExp()
        {
            Emisor emisor = new Emisor(
                        NombreComercial: "DEMO",
                        NombreLegal: "DEMO S.A.C",
                        TipoDocumentoIdentidad: Emisor.TipoDocumentoIdentidadEnum.Ruc,
                        NumeroDocumentoIdentidad: "10324047366",
                        Correo: "emisorCliente@close2u.pe",
                        DomicilioFiscal: new DomicilioFiscal(
                            Ubigeo: "150113",
                            Direccion: "AV. REPUBLICA DE CHILE NRO. 751 DPTO. C",
                            Urbanizacion: "URB. SANTA BEATRIZ",
                            Provincia: "Lima",
                            Distrito: "Jesus Maria",
                            Departamento: "Lima",
                            Pais: DomicilioFiscal.PaisEnum.Peru
                            )
                        );


            /*****************Receptor********************/
            Receptor receptor = new Receptor(
                      NombreComercial: "ASOCIACION CIVIL SANTA MARIA",
                      NombreLegal: "ASOCIACION CIVIL SANTA MARIA",
                      TipoDocumentoIdentidad: Receptor.TipoDocumentoIdentidadEnum.Ruc,
                      NumeroDocumentoIdentidad: "20160394031",
                      Correo: "receptorCliente@close2u.pe",
                      DomicilioFiscal: new DomicilioFiscal(
                          Ubigeo: "150113",
                          Direccion: "CALLE LIZARDO ALZAMORA 193",
                          Urbanizacion: "URB. SANTA BEATRIZ",
                          Provincia: "Lima",
                          Distrito: "SAN ISIDRO",
                          Departamento: "Lima",
                          Pais: DomicilioFiscal.PaisEnum.Peru
                          )
                      );




            InformacionAdicional informacion = new InformacionAdicional();
            informacion.TipoOperacion = InformacionAdicional.TipoOperacionEnum.VentaInterna;

            Factura factura = new Factura(Close2u: new Integracion("C2U-2",
                                            Integracion.TipoIntegracionEnum.Offline, null,
                                            Integracion.TipoRegistroEnum.SinIgv),
                DatosDocumento: new Principal(FechaEmision: "2016-11-01",
                                                Serie: "FF93", Numero: 5,
                                                Moneda: Principal.MonedaEnum.Pen)
           ,
                Emisor: emisor,
                Receptor: receptor,
                DetalleDocumento: new List<DocumentoItem>(),
                InformacionAdicional: informacion);
            /*****************Totales********************/
            //descuentoGlobal
            factura.DescuentoGlobal = 0.0;
            factura.DetalleDocumento = new List<DocumentoItem>();
            /********************Inicio item1***************************/
            DocumentoItem facItem1 = new DocumentoItem(
                UnidadMedida: DocumentoItem.UnidadMedidaEnum.Caja,
                NumeroOrden: factura.DetalleDocumento.Count + 1,
                Cantidad: 5.0,
                Descripcion: "LAPICERO' PUNTA FINA 1",
                ValorVentaUnitarioItem: 11.9,
                CodigoProducto: "PROD1",
                TipoAfectacion: DocumentoItem.TipoAfectacionEnum.GravadoOperacionOnerosa
                );

            factura.DetalleDocumento.Add(facItem1);
            return factura;
        }
    }
}
