﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;

using IO.Swagger.Model;
namespace DemoConnector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Factura factura = DemoConnector.CasosFacturasYNotas.G3_Grat_FF13.Caso1FAGratuitas.getCasePrueba1FAGratui();
       //     Boleta boleta = DemoConnector.CasosBoletasYNotas.Caso1_BOGravBB11.getCasePrueba1_BOGrav();

            Respuesta respuesta = null;
            try
            {
                ComprobantecontrollerApi api = new ComprobantecontrollerApi();

                api.Configuration.ApiClient = new ApiClient("https://c2udev.invoice2u.pe/apiemisor");
                if (!api.Configuration.DefaultHeader.ContainsKey("j_ruc"))
                {//usuario sergio =10461633876
                    api.Configuration.AddDefaultHeader("j_ruc","10461633876");
                    api.Configuration.AddDefaultHeader("j_user","10461633876");
                    api.Configuration.AddDefaultHeader("j_password","10461633876");
                }

                respuesta = api.RegistrarFacturaUsingPUT(factura);
         

            }
            catch (ApiException ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (respuesta != null)
                    MessageBox.Show(respuesta.TipoDocumento + "-" + respuesta.Serie + "-" + respuesta.Numero);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            //  Factura factura = DemoConnector.CasosFacturasYNotas.G3_Grat_FF13.Caso1FAGratuitas.getCasePrueba1FAGratui();
            Boleta boleta = DemoConnector.CasosBoletasYNotas.G3_Grat_BB13.Caso4_BOG3Gratuita.getCasePrueba4_BOGrat();
            Respuesta respuesta = null;
            try
            {
                ComprobantecontrollerApi api = new ComprobantecontrollerApi();

                api.Configuration.ApiClient = new ApiClient("https://c2udev.invoice2u.pe/apiemisor");
                if (!api.Configuration.DefaultHeader.ContainsKey("j_ruc"))
                {//usuario sergio =10461633876
                    api.Configuration.AddDefaultHeader("j_ruc", "10461633876");
                    api.Configuration.AddDefaultHeader("j_user", "10461633876");
                    api.Configuration.AddDefaultHeader("j_password", "10461633876");
                }

                respuesta = api.RegistrarBoletaUsingPUT1(boleta);
                //       respuesta = api.RegistrarFacturaUsingPUT(boleta;

            }
            catch (ApiException ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (respuesta != null)
                    MessageBox.Show(respuesta.TipoDocumento + "-" + respuesta.Serie + "-" + respuesta.Numero);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {


            NotaCredito notacredito = DemoConnector.CasosFacturasYNotas.G1_Grav_FF11.Caso7FANCG1Gravadas.getCase7FANCG1GRAV();
            Respuesta respuesta = null;
            try
            {
                ComprobantecontrollerApi api = new ComprobantecontrollerApi();

                api.Configuration.ApiClient = new ApiClient("https://c2udev.invoice2u.pe/apiemisor");
                if (!api.Configuration.DefaultHeader.ContainsKey("j_ruc"))
                {//usuario sergio =10461633876
                    api.Configuration.AddDefaultHeader("j_ruc", "10461633876");
                    api.Configuration.AddDefaultHeader("j_user", "10461633876");
                    api.Configuration.AddDefaultHeader("j_password", "10461633876");
                }

                respuesta = api.RegistrarNotaCreditoUsingPUT(notacredito);
                //       respuesta = api.RegistrarFacturaUsingPUT(boleta;

            }
            catch (ApiException ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (respuesta != null)
                    MessageBox.Show(respuesta.TipoDocumento + "-" + respuesta.Serie + "-" + respuesta.Numero);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ResumenBaja resumenbaja = DemoConnector.Resumenbajatesst.Caso1ComBajasTestCase.getDataTestCase();
            Respuesta respuesta =null;
            try
            {
                ComprobantecontrollerApi api = new ComprobantecontrollerApi();

                api.Configuration.ApiClient = new ApiClient("https://c2udev.invoice2u.pe/apiemisor");
                if (!api.Configuration.DefaultHeader.ContainsKey("j_ruc"))
                {//usuario sergio =10461633876
                    api.Configuration.AddDefaultHeader("j_ruc", "10461633876");
                    api.Configuration.AddDefaultHeader("j_user", "10461633876");
                    api.Configuration.AddDefaultHeader("j_password", "10461633876");
                }

                respuesta = api.RegistrarResumenBajaUsingPUT1(resumenbaja);
                 

            }
            catch (ApiException ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (respuesta != null)
                    MessageBox.Show(respuesta.TipoDocumento + "-" + respuesta.Serie + "-" + respuesta.Numero);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ResumenDiario resumendiario = DemoConnector.ResumenDiariotest.Caso1ComDiarioTestCase.getDataTestCase();
            Respuesta respuesta = null;
            try
            {
                ComprobantecontrollerApi api = new ComprobantecontrollerApi();

                api.Configuration.ApiClient = new ApiClient("https://c2udev.invoice2u.pe/apiemisor");
                if (!api.Configuration.DefaultHeader.ContainsKey("j_ruc"))
                {
                    api.Configuration.AddDefaultHeader("j_ruc", "10461633876");
                    api.Configuration.AddDefaultHeader("j_user", "10461633876");
                    api.Configuration.AddDefaultHeader("j_password", "10461633876");
                }

                respuesta = api.RegistrarResumenDiarioUsingPUT(resumendiario);
               

            }
            catch (ApiException ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (respuesta != null)
                    MessageBox.Show(respuesta.TipoDocumento + "-" + respuesta.Serie + "-" + respuesta.Numero);
            }
        }

      
    }
    }

