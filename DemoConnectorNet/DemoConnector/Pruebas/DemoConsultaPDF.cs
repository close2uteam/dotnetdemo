﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Security;

using System.Configuration.Assemblies;
namespace DemoConnector.Pruebas
{
 public   class DemoConsultaPDF
    {
        

    public static void main(String[] args)
        {


            ComprobantecontrollerApi comprobantecontrollerApi = new ComprobantecontrollerApi();

            ApiClient apiClient = comprobantecontrollerApi.Configuration.ApiClient;
            apiClient.ParameterToString(Constantes.Url);
            
            
            try
            {
                apiClient.Configuration.AddDefaultHeader("j_ruc", Constantes.RUC);
                apiClient.Configuration.AddDefaultHeader("j_user", Constantes.User);
                apiClient.Configuration.AddDefaultHeader("j_password", Constantes.Password);
            }
            catch (Exception ex) { }


            comprobantecontrollerApi.Configuration.setApiClientUsingDefault(apiClient);

            String respuesta = null;
          
            try
            {
                DocumentoInfo documento = new DocumentoInfo("20549776974", "FF01", "1", "01");
               
                respuesta = comprobantecontrollerApi.ConsultarPdfUsingPUT(documento);

            }
            catch (ApiException ex)
            {
                Console.WriteLine("Caso12, de ID:" + ex.ErrorContent.getResponseBody() + "error");
                Console.WriteLine("Caso12, de ID:" + ex.ErrorContent.getStackTrace() + "error");
            }
            catch (Exception ex)
            {
               // Console.WriteLine("Caso12, de ID:" + ex.getStackTrace() + "error");
            }
            finally
            {
                if (respuesta != null)
                    Console.WriteLine("Caso12, de ID:" + respuesta);
            }

        }
    }
}
