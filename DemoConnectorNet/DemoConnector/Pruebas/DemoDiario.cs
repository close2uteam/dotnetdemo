﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Security;

namespace DemoConnector.Pruebas
{
   public class DemoDiario
    {
        public static void main(String[] args)
        {


            ComprobantecontrollerApi comprobantecontrollerApi = new ComprobantecontrollerApi();

            ApiClient apiClient = comprobantecontrollerApi.Configuration.ApiClient;
            apiClient.ParameterToString(Constantes.Url);


            try
            {
                apiClient.Configuration.AddDefaultHeader("j_ruc", Constantes.RUC);
                apiClient.Configuration.AddDefaultHeader("j_user", Constantes.User);
                apiClient.Configuration.AddDefaultHeader("j_password", Constantes.Password);
            }
            catch (Exception ex) { }


            comprobantecontrollerApi.Configuration.setApiClientUsingDefault(apiClient);

            Respuesta respuesta = null;

            try
            {
                ResumenDiario resumenDiario = DemoConnector.ResumenDiariotest.Caso1ComDiarioTestCase.getDataTestCase();

                respuesta = comprobantecontrollerApi.RegistrarResumenDiarioUsingPUT(resumenDiario);

            }
            catch (ApiException ex)
            {
                Console.WriteLine("Caso12, de ID:" + ex.ErrorContent.getResponseBody() + "error");
                Console.WriteLine("Caso12, de ID:" + ex.ErrorContent.getStackTrace() + "error");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Caso12, de ID:" + ex.Message + "error");
            }
            finally
            {
                if (respuesta != null)
                    Console.WriteLine("Caso12, de ID:" + respuesta.TipoDocumento + respuesta.Identificador + respuesta.Serie + "-" + respuesta.Numero);
            }

        }
    }
}
