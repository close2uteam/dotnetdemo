﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Model;
using IO.Swagger.Api;
using IO.Swagger.Client;
using DemoConnector.Pruebas;
using DemoConnector.Properties;

namespace DemoConnector.CasosBoletasYNotas
{
   public class Caso1_BOGravBB11
    {
        public static Boleta getCasePrueba1_BOGrav()
        {
            /*************emisor**********************/
                        Emisor emisor = new Emisor(
                        NombreComercial: "Asociacion Circulo Militar del Peru",
                        NombreLegal: "ASOCIACION CIRCULO MILITAR DEL PERU",

                            TipoDocumentoIdentidad: Emisor.TipoDocumentoIdentidadEnum.Ruc,
                            NumeroDocumentoIdentidad: "10461633876",
                            
                            Correo: "sergio.conez@close2u.pe",
                            DomicilioFiscal: new DomicilioFiscal(

                            Direccion: "AV. SALAVERRY NRO. 1650 - Lima Lima  Jesús María",
                            Urbanizacion: "",
                            Provincia: "Lima",
                            Distrito: "Jesus Maria",
                            Departamento: "Lima",
                            Pais: DomicilioFiscal.PaisEnum.Peru
                            )
                        );

            /****************Receptor********************/
            Receptor receptor = new Receptor(
                  NombreComercial: "luis Menage S.A.C",
                  NombreLegal: "luis Menage S.A.C",

                  TipoDocumentoIdentidad: Receptor.TipoDocumentoIdentidadEnum.Ruc,
                  NumeroDocumentoIdentidad: "10461633876",
                  Correo: "sergio.conez@close2u.pe",
                  DomicilioFiscal: new DomicilioFiscal(

                      Direccion: "CALLE CAHUIDE Nº900  BLOCK 19 DPTO ",
                      Urbanizacion: "URB. SAN FELIPE",
                      Provincia: "Lima",
                      Distrito: "JESUS MARIA",
                      Departamento: "Lima",
                      Pais: DomicilioFiscal.PaisEnum.Peru

                      )
                  );




            /***********************Principal**********************************/

            InformacionAdicional informacion = new InformacionAdicional();
            informacion.TipoOperacion = InformacionAdicional.TipoOperacionEnum.VentaInterna;

            Boleta boleta = new Boleta(Close2u: new Integracion("C2U-2",
                                            Integracion.TipoIntegracionEnum.Erp, null,
                                            Integracion.TipoRegistroEnum.SinIgv)
                                            ,
                DatosDocumento: new Principal(FechaEmision: "2016-11-07",
                                                Serie: "BB12", Numero: 5,
                                                Moneda: Principal.MonedaEnum.Pen,
                                                   Glosa: "aqui se vera las observaciones en la ",
                                                   //Esta noleta el pago es al contado
                                                     FormaPago: "Boleta  Nota credito contado")
           ,
                Emisor: emisor,
                Receptor :receptor,
                DetalleDocumento: new List<DocumentoItem>(),
                InformacionAdicional: informacion);
            /*****************Totales********************/
            //descuentoGlobal
            boleta.DescuentoGlobal = 0.0;
            boleta.DetalleDocumento = new List<DocumentoItem>();
            /********************Inicio item1***************************/
            DocumentoItem facItem1 = new DocumentoItem(
                UnidadMedida: DocumentoItem.UnidadMedidaEnum.Caja,
                NumeroOrden: boleta.DetalleDocumento.Count + 1,
                Cantidad: 2.0,
                Descripcion: "Chip movistar",
                ValorVentaUnitarioItem: 70.70,
                CodigoProducto: "PROD1",
                TipoAfectacion: DocumentoItem.TipoAfectacionEnum.GravadoOperacionOnerosa,
                PrecioVentaUnitarioItem: 70.70
                );
            DocumentoItem facItem2 = new DocumentoItem(
              UnidadMedida: DocumentoItem.UnidadMedidaEnum.Caja,
              NumeroOrden: boleta.DetalleDocumento.Count + 1,
              Cantidad: 4.0,
              Descripcion: "Gaseosa Inca",
              PrecioVentaUnitarioItem: 10.92,
              CodigoProducto: "PROD1",
              TipoAfectacion: DocumentoItem.TipoAfectacionEnum.GravadoOperacionOnerosa
              
              );




            boleta.DetalleDocumento.Add(facItem1);
            boleta.DetalleDocumento.Add(facItem2);

            return boleta;
        }
    }

}
