﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IO.Swagger.Model;
using IO.Swagger.Api;
using IO.Swagger.Client;
using DemoConnector.Pruebas;
using DemoConnector.Properties;

namespace DemoConnector.CasosBoletasYNotas
{
   public class Caso2_1BOGravBB11
    {
        public static Boleta getCasePrueba2_1BOGrav()
        {
            /*************emisor**********************/
            Emisor emisor = new Emisor(
                        NombreComercial: "Asociacion Circulo Militar del Peru",
                        NombreLegal: "ASOCIACION CIRCULO MILITAR DEL PERU",

                            TipoDocumentoIdentidad: Emisor.TipoDocumentoIdentidadEnum.Ruc,
                            NumeroDocumentoIdentidad: "10461633876",

                            Correo: "sergio.conez@close2u.pe",
                            DomicilioFiscal: new DomicilioFiscal(

                            Direccion: "AV. SALAVERRY NRO. 1650 - Lima Lima  Jesús María",
                            Urbanizacion: "",
                            Provincia: "Lima",
                            Distrito: "Jesus Maria",
                            Departamento: "Lima",
                            Pais: DomicilioFiscal.PaisEnum.Peru
                            )
                        );
            /****************Receptor********************/
            Receptor receptor = new Receptor(
                  NombreComercial: "luis Menage S.A.C",
                  NombreLegal: "luis Menage S.A.C",

                  TipoDocumentoIdentidad: Receptor.TipoDocumentoIdentidadEnum.Ruc,
                  NumeroDocumentoIdentidad: "10461633876",
                  Correo: "sergio.conez@close2u.pe",
                  DomicilioFiscal: new DomicilioFiscal(

                      Direccion: "CALLE CAHUIDE Nº900  BLOCK 19 DPTO ",
                      Urbanizacion: "URB. SAN FELIPE",
                      Provincia: "Lima",
                      Distrito: "JESUS MARIA",
                      Departamento: "Lima",
                      Pais: DomicilioFiscal.PaisEnum.Peru

                      )
                  );




            /***********************Principal**********************************/


            InformacionAdicional informacion = new InformacionAdicional();
            informacion.TipoOperacion = InformacionAdicional.TipoOperacionEnum.VentaInterna;

           Boleta boleta = new Boleta(Close2u: new Integracion("C2U-2",
                                            Integracion.TipoIntegracionEnum.Erp, null,
                                            Integracion.TipoRegistroEnum.SinIgv),
                DatosDocumento: new Principal(FechaEmision: "2016-11-07",
                                                Serie: "BB13", Numero: 5,
                                                Moneda: Principal.MonedaEnum.Pen,
                                                 Glosa: "aqui se vera las observaciones en la ",
                                                 //esta boleta esta con numero de coutas para el credito
                                                  FormaPago: "Boleta  Nota credito credito"
                                                )
           ,
                Emisor: emisor,
                Receptor:receptor,
                DetalleDocumento: new List<DocumentoItem>(),
                InformacionAdicional: informacion);
            /*****************Totales********************/
            //descuentoGlobal
            boleta.DescuentoGlobal = 0.0;
            boleta.DetalleDocumento = new List<DocumentoItem>();
            /********************Inicio item1***************************/
            DocumentoItem facItem1 = new DocumentoItem(
                UnidadMedida: DocumentoItem.UnidadMedidaEnum.Caja,
                NumeroOrden: boleta.DetalleDocumento.Count + 1,
                Cantidad: 17.0,
                Descripcion: "Gaseosa Coca",
                ValorVentaUnitarioItem: 24.80,
                CodigoProducto: "PROD1",
                TipoAfectacion: DocumentoItem.TipoAfectacionEnum.ExoneradoOperacionOnerosa,
                PrecioVentaUnitarioItem: 156.00
                );
      


            boleta.DetalleDocumento.Add(facItem1);
            

            return boleta;
        }
    }

}
