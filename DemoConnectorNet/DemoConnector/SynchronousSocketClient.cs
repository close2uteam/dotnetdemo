﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace DemoConnector
{
  public  class SynchronousSocketClient
    {

        public  String InicializarCliente(string mensaje)
        {
            string resultado ="";
           
            byte[] bytes = new byte[130];

         
                
            try
            {
               
                System.Net.IPAddress ipAddress = System.Net.IPAddress.Parse("192.168.1.8");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 15000);

              
                Socket sender = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

               
                try
                {
                    sender.Connect(remoteEP);

                    Console.WriteLine("Socket connected to {0}",
                        sender.RemoteEndPoint.ToString());

                  
                    byte[] msg = Encoding.ASCII.GetBytes(mensaje);

                    
                    int bytesSent = sender.Send(msg);

                    
                   

                   Recibir(sender, bytes, 0, bytes.Length, 10000);
                    resultado = Encoding.ASCII.GetString(bytes, 0, bytes.Length);
                    


                   
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return resultado;
        }

        public  void Recibir(Socket socket, byte[] buffer, int offset, int size, int timeout)
        {

            int startTickCount = Environment.TickCount;
            int received = 0;  
            do
            {
                if (Environment.TickCount > startTickCount + timeout)
                    throw new Exception("Timeout.");
                try
                {
                    received += socket.Receive(buffer, offset + received, size - received, SocketFlags.None);
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.WouldBlock ||
                        ex.SocketErrorCode == SocketError.IOPending ||
                        ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                    {
                       
                        Thread.Sleep(30);
                    }
                    else
                        throw ex;  
                }
            } while (received < size);
        }

    }

}
